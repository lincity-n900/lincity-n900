/*
Copyright (C) 2005 Matthias Braun <matze@braunis.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifdef GLES
#ifndef __TEXTUREGL_HPP__
#define __TEXTUREGL_HPP__

#include <GLES/gl.h>
#include "gui/Rect2D.hpp"
#include "gui/Texture.hpp"

class TextureGLES : public Texture
{
public:
    virtual ~TextureGLES();

    float getWidth() const
    {
        return width;
    }

    float getHeight() const
    {
        return height;
    }

private:
    friend class PainterGLES;
    friend class TextureManagerGLES;

    TextureGLES(GLuint newhandle);
    
    GLuint handle;
    float width, height;
    Rect2D rect;
};

#endif


#endif /* GLES */

