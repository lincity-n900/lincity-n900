/*
Copyright (C) 2005 Wolfgang Becker <uafr@gmx.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <config.h>

#include "Config.hpp"
#include "gui/XmlReader.hpp"
#include "PhysfsStream/PhysfsStream.hpp"
#include "lincity/engglobs.h"

#include <assert.h>
#include <iostream>

#include <cstdlib>
#include <string.h>

#include <iostream>
#include <fstream>

Config* configPtr = 0;

Config *getConfig()
{
    if(configPtr == 0)
        configPtr = new Config();

    return configPtr;
}

Config::Config()
{
    assert(configPtr == 0);

    //Default Values
    useOpenGLES = false;
    useFullScreen = true;
    videoX = 800; 
    videoY = 480;

    soundVolume = 100;
    musicVolume = 50;
    soundEnabled = true;
    musicEnabled = true;
    restartOnChangeScreen = true;
     
    //#define MONTHGRAPH_W 120 
    //#define MONTHGRAPH_H 64
    monthgraphW = 190;
    monthgraphH = 64;
    skipMonthsFast = 1;
    quickness = FAST_TIME_FOR_YEAR;

    playSongName = "02 - Robert van Herk - City Blues.ogg";
    //First we load the global File which should contain
    try {
        //sane defaults for the local system.
        load( "lincityconfig.xml" );
    } catch(std::exception& e) {
#ifdef DEBUG
        std::cerr << "Couldn't load lincityconfig.xml: " << e.what() << "\n";
#endif
    }
    //Then load another file where to overwrite some values.
    try {
        load( "userconfig.xml" );
    } catch(std::exception& e) {
#ifdef DEBUG
        std::cerr << "Couldn't load userconfig.xml: " << e.what() << "\n";
#endif
    }
}

Config::~Config()
{
    if( configPtr == this )
    {
        configPtr = 0;
    }
}

/*
 * Read Integer Value from char-Array.
 * use defaultValue on Errors or if Value is not in given Interval.
 */
int Config::parseInt( const char* value, int defaultValue, int minValue, int maxValue ) {
    int tmp; 
    if(sscanf(value, "%i", &tmp) != 1) 
    {
        std::cerr << "Config::parseInt# Error parsing integer value '" << value << "'.\n";
        tmp = defaultValue;
    }
    if( ( tmp >= minValue ) && ( tmp <= maxValue ) ) {
        return tmp;
    } else {
        std::cerr << "Config::parseInt# Value '" << value << "' not in ";
        std::cerr << minValue << ".." << maxValue << "\n";
        return defaultValue;
    }
}
/*
 * Load configuration from File.
 */
void Config::load( const std::string& filename ){
    XmlReader reader( filename );
    while( reader.read() ) {
        if( reader.getNodeType() == XML_READER_TYPE_ELEMENT) 
        {
            const std::string& element = (const char*) reader.getName();
            if( element == "video" ) {
                XmlReader::AttributeIterator iter(reader);
                while(iter.next()) 
                {
                    const char* name = (const char*) iter.getName();
                    const char* value = (const char*) iter.getValue();
                    if( strcmp( name, "useOpenGLES" ) == 0 ) {
                        useOpenGLES  = parseBool(value, false);
                    } else if( strcmp(name, "x" ) == 0 ) {
                        videoX = parseInt( value, 800, 10 );
                    } else if(strcmp(name, "y") == 0 ) {
                        videoY = parseInt( value, 480, 10 );
                    } else if(strcmp(name, "fullscreen") == 0) {
                        useFullScreen = parseBool(value, false);
                    } else if(strcmp(name, "restartOnChangeScreen") == 0) {
                        restartOnChangeScreen = parseBool(value, true);
                    } else {
                        std::cerr << "Config::load# Unknown attribute '" << name;
                        std::cerr << "' in element '" << element << "' from " << filename << ".\n";
                    }
                }
            } else if ( element == "audio" ) {
                XmlReader::AttributeIterator iter(reader);
                while(iter.next()) 
                {
                    const char* name = (const char*) iter.getName();
                    const char* value = (const char*) iter.getValue();
                    if(strcmp(name, "soundVolume" ) == 0) {
                        soundVolume = parseInt(value, 100, 0, 100);
                    } else if(strcmp(name, "musicVolume") == 0) {
                        musicVolume = parseInt(value, 100, 0, 100);
                    } else if(strcmp(name, "soundEnabled") == 0) {
                        soundEnabled = parseBool(value, true);
                    } else if(strcmp(name, "musicEnabled") == 0) {
                        musicEnabled = parseBool(value, true);
                    } else if(strcmp(name, "playSongName") == 0) {
                        playSongName = value;
                    } else {
                        std::cerr << "Config::load# Unknown attribute '" << name;
                        std::cerr << "' in element '" << element << "' from " << filename << ".\n";
                    }
                }
            } else if ( element == "game" ) {
                XmlReader::AttributeIterator iter(reader);
                while(iter.next()) 
                {
                    const char* name = (const char*) iter.getName();
                    const char* value = (const char*) iter.getValue();
                    if(strcmp(name, "skipMonthsFast" ) == 0) {
                        skipMonthsFast = parseInt( value, 1, 1 );
                    } else if( strcmp(name, "monthgraphW" ) == 0 ){
                       monthgraphW  = parseInt(value, 120, 0);
                    } else if( strcmp(name, "monthgraphH" ) == 0 ){
                       monthgraphH  = parseInt(value, 64, 0);
                    } else if( strcmp(name, "quickness" ) == 0 ){
                        quickness = parseInt(value, 9, 1, 9);
                    } else {
                        std::cerr << "Config::load# Unknown attribute '" << name;
                        std::cerr << "' in element '" << element << "' from " << filename << ".\n";
                    }
                }
            } else {
                std::cerr << "Config::load# Unknown element '" << element << "' in "<< filename << ".\n";
            }
        }
    }
}

bool
Config::parseBool(const char* value, bool defaultValue)
{
    if(strcmp(value, "no") == 0 || strcmp(value, "off") == 0
            || strcmp(value, "false") == 0 || strcmp(value, "NO") == 0
            || strcmp(value, "OFF") == 0 || strcmp(value, "FALSE") == 0) {
        return false;
    }
    if(strcmp(value, "yes") == 0 || strcmp(value, "on") == 0
            || strcmp(value, "true") == 0 || strcmp(value, "YES") == 0
            || strcmp(value, "ON") == 0 || strcmp(value, "TRUE") == 0) {
        return true;
    }

    std::cerr << "Couldn't parse boolean value '" << value << "'.\n";
    return defaultValue;
}

/*
 * Save configuration to File.
 */
void
Config::save(){

/*
Maemo5: this is seg faulting with:
OFileStream userconfig( "userconfig.xml" );
#0  0x40329cd8 in std::ostream::sentry::sentry(std::ostream&) () from /usr/lib/libstdc++.so.6
#1  0x4032aed8 in std::basic_ostream<char, std::char_traits<char> >& std::__ostream_insert<char, std::char_traits<char> >(std::basic_ostream<char, 
std::char_traits<char> >&, char const*, int) () from /usr/lib/libstdc++.so.6
#2  0x00065f84 in Config::save (this=0x204df8)
    at 
/scratchbox/compilers/cs2007q3-glibc2.5-arm7/bin/../lib/gcc/arm-none-linux-gnueabi/4.2.1/../../../../arm-none-linux-gnueabi/include/c++/4.2.1/ostream:517
#3  0x0003a160 in main (argc=5, argv=0xbe8bc674) at src/lincity-ng/main.cpp:427

This happens when << is called.
*/

    //MAEMO5: hard coded path
    std::ofstream userconfig;
    userconfig.open ("/home/user/.lincity-ng/userconfig.xml");

    userconfig << "<?xml version=\"1.0\"?>\n";
    userconfig << "<configuration>\n";
    userconfig << "    <video x=\"" << videoX << "\" y=\"" << videoY << "\" useOpenGLES=\"" 
        << (useOpenGLES?"yes":"no") << "\" fullscreen=\"" << (useFullScreen?"yes":"no")  
        << "\" />\n";
    userconfig << "    <audio soundEnabled=\"" << (soundEnabled?"yes":"no")  
        << "\" soundVolume=\"" << soundVolume << "\" \n";
    userconfig << "           musicEnabled=\"" << (musicEnabled?"yes":"no")  
        << "\" musicVolume=\"" << musicVolume << "\"\n";
    userconfig << "           playSongName=\"" << playSongName << "\" />\n";
    userconfig << "    <game quickness=\""<< quickness <<"\" />\n";
    userconfig << "</configuration>\n";

    userconfig.close();
}

